# README

levantar el servidor del backend en el puerto 3000:

rails server -p 3000

crear la base de datos:

rails db:create

correr las migraciones:

rails db:migrate

correr los seeds:

rails db:seed
