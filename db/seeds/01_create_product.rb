branches = [
  {branch:  'santa anna',                  description: 'una descripcion con la palabra orejero'},
  {branch:  'mantequilla apocopa',         description: 'una descripcion con la palabra rotor'},
  {branch:  'rayar dos',                   description: 'una descripcion con la palabra selles'},
  {branch:  '3 sagas',                     description: 'una descripcion con la palabra menem'},
  {branch:  'rodador de pisos',            description: 'una descripcion con la palabra seres'},
  {branch:  'ceramica arenera',            description: 'una descripcion con la palabra sometemos'},
  {branch:  'espejo para reconocer',       description: 'una descripcion con la palabra osos'},
  {branch:  'lechugas sanas',              description: 'una descripcion con la palabra anana'},
  {branch:  'barco kayak',                 description: 'una descripcion con la palabra arepera'},
  {branch:  'radar optico',                description: 'una descripcion sin palindromes'},
  {branch:  'cuchillo para rajar',         description: 'una descripcion sin palindromes'},
  {branch:  'cuentos que se narran solos', description: 'una descripcion sin palindromes'},
  {branch:  'somos tu y yo',               description: 'una descripcion sin palindromes'},
  {branch:  'carros en venta',             description: 'una descripcion sin palindromes'},
  {branch:  'lapicero azul',               description: 'una descripcion sin palindromes'},
  {branch:  'cuna para bebe',              description: 'una descripcion con la palabra anilina'},
]

Product.destroy_all

branches.each do |item|
  p '-'*90
  Product.create(
    branch: item[:branch], 
    description: item[:description],
    price:  rand(10..150), 
  )
  p item
end