class ProductsController < ApplicationController

  def search
    render json: Product.is_palindrome?(params[:word])
  end

end
