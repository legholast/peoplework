class Product < ApplicationRecord

  def self.is_palindrome? word
    palindrome = word == word.reverse
    Product
    .where("branch ILIKE ? OR description ILIKE ?", "%#{word}%", "%#{word}%")
    .as_json
    .map do |prod|
      {
        offer: palindrome ? (prod['price'].to_f)/2 : nil,
        price: prod['price'],
        branch: prod['branch'],
        description: prod['description'],
      }
    end
  end

end
